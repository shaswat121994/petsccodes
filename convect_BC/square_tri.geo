// unstructured triangular mesh
n = 100;

Point(1) = {0, 0, 0, 1.0/n};
Point(2) = {1, 0, 0, 1.0/n};
Point(3) = {1, 1, 0, 1.0/n};
Point(5) = {0, 1, 0, 1.0/n};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 5};
Line(4) = {5, 1};

Curve Loop(1) = {4, 1, 2, 3};
Plane Surface(1) = {1};

Physical Curve(1) = {1};
Physical Curve(2) = {2};
Physical Curve(3) = {3};
Physical Curve(4) = {4};
Physical Surface(2) = {1};

Transfinite Surface {1};
