# README #

Convect EU: First order linear advection with a function for Forward Euler timestepping

Convect TS: First order linear advection with TS context used for timestepping

Convect BC: Implementation of boundary condition using boundary labels from gmsh

Compile:

	make convect

Run:

	// Read mesh from a Gmsh file
	mpiexec -n <numproc> ./convect -mesh <GmshFile>
	or
	// Generate own mesh
	mpiexec -n <numproc> ./convect -nx <NumCellsX> -ny <NumCellsY>

Options:
   
	-tf  Final time
	-vtk Saving frequency for vtk files
